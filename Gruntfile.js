module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    strip_code: {
      options: {
        start_comment: "test-code",
        end_comment: "end-test-code",
      },
      your_target: {
        // a list of files you want to strip code from
        src: "dist/<%= pkg.name %>.js",
        src: "dist/<%= pkg.name %>.configured.js",
        src: "dist/<%= pkg.name %>-meteor.js"
      }
    },

    concat: {
      options: {
        separator: '\n\n',
      },
      configured : {
        src: ['src/BoolEditLanguage.js',
              'src/BoolEditProof.js',
              'src/BoolEditRules.js',
              'src/BoolEditConfig.js',
              'src/BoolEditSetup.js'],
        dest: 'dist/<%= pkg.name %>.configured.js',
      },
      dist: {
        src: ['src/BoolEditLanguage.js',
              'src/BoolEditProof.js',
              'src/BoolEditRules.js'],
        dest: 'dist/<%= pkg.name %>.js',
      },
      meteor: {
        src: ['src/MeteorLogo',
              'src/BoolEditLanguage.js',
              'src/BoolEditProof.js',
              'src/BoolEditRules.js',
              'src/BoolEditConfig.js',
              'src/BoolEditSetup.js',
              'src/MeteorSetup.js'],
        dest: 'dist/<%= pkg.name %>-meteor.js',
      },

    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'dist/<%= pkg.name %>.js',
        dest: 'dist/<%= pkg.name %>.min.js'
      },
      build: {
        src: 'dist/<%= pkg.name %>.configured.js',
        dest: 'dist/<%= pkg.name %>.configured.min.js'
      }
    },

    watch: {
      js: {
        files: ['src/*.js'],
        tasks: ['jshint', 'concat', 'strip_code', 'uglify']
      }
    },

    jshint: {
      files : ['src/*.js', 'test/spec/*.js'],
    }

  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-strip-code');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  // Default task(s).
  grunt.registerTask('default', ['jshint', 'concat', 'strip_code', 'uglify']);

};
