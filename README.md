# README

## Synopsis

 BoolEdit.js is a Javascript library that marks Boolean algebra proofs. It handles parsing of user strings into mathematical formulae, as well as evaluating proofs line-by-line to determine whether the rule cited correctly justifies the statement made at that line. The connectives and rules used are fully user configurable by feeding the appropriate modules a JSON object.

## Motivation

For instructors, marking student proofs by hand is tedious, very slow, and totally algoritmic. In other words, a perfect job for a computer. For the student, BoolEdit reduces the time between making a mistake and knowing it from a week to a second.

## Modules

BoolEdit-JS is arranged into three main modules.

### BoolEditLanguage

BoolEditLanguage defines some private classes that represent formulae in Boolean algebra. Its main public function is formulaFactory, which takes strings and parses them into these BoolEdit formula objects. You can configure the set of connectives it uses by calling its defineOperators() function.

### BoolEditRules

BoolEditRules provides an interface for defining and storing Boolean algebraic rules. You can configure the set of rules it uses with its defineRules() function.

### BoolEditProof

BoolEditProof provides an interface for creating, extending and evaluating proofs in Boolean algebra.

### BoolEditConfig

BoolEditConfig is a fourth supplementary module that exists simply to give some ready-made config data for BoolEdit in the form of a few different kinds of JS objects. Its data is patterned after the way that Boolean algebra is taught at BCIT. You can configure BoolEdit to your own specs by creating analagous JS objects and feeding them to the appropriate BoolEdit modules.

## Code Example

```
expect(BoolEditRules.getRule('T13b').equivalence).toEqual("x = x''");

var foo = new BoolEditProof({
    equivalence: "p+q=p''+q"
});

foo.addLine({formula: 'p + q'});

expect(foo.toJSObject()).toEqual({
    equivalence: "p+q=p''+q",
    isComplete: false,
    lines: [ Object({ formula: 'p+q', rule: undefined, evaluation: true }) ]
});

foo.addLine({formula: "p'' + q",
             rule: 'T13a'});

expect(foo.toJSObject()).toEqual({
    equivalence: "p+q=p''+q",
    isComplete: true,
    lines: [ Object({ formula: 'p+q', rule: undefined, evaluation: true }),
             Object({ formula: "p''+q", rule: 'T13a', evaluation: true }) ]
});

```

## Getting started / running the test drivers

All components needed to run the tests are listed in package.json and can be installed with the Node Package Manager by entering `npm install` in a terminal window at the project root.

Once this is finished, you can run all the tests by simply opening test/SpecRunner.html in a browser.

## Building the minified files with Grunt

BoolEdit is set up so that running Grunt from the project root creates minified versions of the library within a single file in dist/. One of these versions, booledit.configured.min.js, is configured with BCIT specs; the other one, booledit.min.js, has no configuration at all and is suitable e.g. for configuration from a remote service.

The Grunt script also removes some access to private members of BoolEdit modules that is present for testing purposes.

All versions of BoolEdit depend on the excellent `he` (HTMLEntities) library to protect against the remote possibility of xss injection via the name of a user-configured rule. For now, to keep things simple, Grunt contatenates the entire he script into both minified versions of BoolEdit.