var BoolEditConfig = ( function() {

    var rulesObject = {

        // Define primitive operator behaviour

        "P2a"  : "0 * 0 = 0",
        "P2b"  : "0 + 0 = 0",
        "P3a"  : "1 * 1 = 1",
        "P3b"  : "1 + 1 = 1",
        "P4a"  : "1 * 0 = 0",
        "P4b"  : "1 + 0 = 1",
        "P5a"  : "1' = 0",
        "P5b"  : "0' = 1",

        // Define algebraic laws

        "L6a"  : "x * y = y * x",
        "L6b"  : "x + y = y + x",
        "L7a"  : "x * (y * z) = (x * y) * z",
        "L7b"  : "x + (y + z) = (x + y) + z",
        "L8a"  : "x * (y + z) = x * y + x * z",
        "L8b"  : "x + (y * z) = (x + y) * (x + z)",

        // Define theorems

        "T9a"  : "x * 0 = 0",
        "T9b"  : "x + 0 = x",
        "T10a" :  "x * 1 = x",
        "T10b" :  "x + 1 = 1",
        "T11a" :  "x * x = x",
        "T11b" :  "x + x = x",
        "T12a" :  "x * x' = 0",
        "T12b" :  "x + x' = 1",
        "T13a" :  "x'' = x",
        "T13b" :  "x = x''",
        "T14a" :  "x + (x * y) = x",
        "T14b" :  "x * (x + y) = x",
        "T14c" :  "x * (x' + y) = x * y",
        "T14d" :  "x + x' * y = x + y",
        "T15a" :  "(x * y)' = x' + y'",
        "T15b" :  "(x + y)' = x' * y'",

        // Define derived operators

        'df. ⊕' : "x ⊕ y = x * y'+ x' * y",
        'df. ⊙' : "x ⊙ y = x * y + x' * y'",
        'df. ↑' : "x ↑ y = (x * y)'",
        'df. ↓' : "x ↓ y = (x + y)'",

    };

    var operators = {
        '+' : { arity: 2, precedence: 1 },
        '*' : { arity: 2, precedence: 2 },
        '\'' : { arity: 1, precedence: 3 },
        '⊕' : { arity: 2, precedence: 0 },
        '⊙' : { arity: 2, precedence: 0 },
        '↑' : { arity: 2, precedence: 0 },
        '↓' : { arity: 2, precedence: 0 }
    };

    var publicInterface = {};

    publicInterface.getOperatorsDefinition = function() {
        return  operators;
    };

    publicInterface.getRulesDefinition = function() {
        return rulesObject;
    };

    return publicInterface;

} )();