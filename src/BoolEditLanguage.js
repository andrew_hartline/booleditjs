// BoolEdit --------------------------------------------------------------------
// Description: Revealing Module to provide a basic API for parsing and
//              representing formulae in Boolean algebra.
// -----------------------------------------------------------------------------
var BoolEditLanguage = ( function() {

    "use strict";

    var myPublicInterface = {}; // public interface, returned at the end

    // Atom --------------------------------------------------------------------
    // Description: A class meant to represent propositional atoms.
    // -------------------------------------------------------------------------
    var Atom = function(letter) {
        validateAsChar(letter);

        if (!isPropositionalAtomSymbol(letter)) {
            throw new Error("Not a valid propositional atom: " + letter);
        }

        var myLetter = letter;

        this.toString = function() {
            return myLetter;
        };
    };

    // Constant ----------------------------------------------------------------
    // Description: A class meant to represent the Boolean constants 1 and 0.,
    // -------------------------------------------------------------------------
    var Constant = function(value) {
        validateAsChar(value);

        if (!isBooleanConstantSymbol(value)) {
            throw new Error("Not a valid Boolean constant: " + value);
        }

        var myValue = value;

        this.toString = function() {
            return myValue;
        };

        this.isLeaf = function() {
            return true;
        };
    };

    // validateAsFormula -------------------------------------------------------
    // Description: Ensures that its input is either a complex formula, atomic
    //              variable or constant expression.
    // Throws:      Error on bogus input
    // -------------------------------------------------------------------------
    var validateAsFormula = function(formula) {
        if (formula.constructor !== ComplexFormula &&
            formula.constructor !== Atom &&
            formula.constructor !== Constant) {
            throw new Error('Not a formula: ' + formula);
        }
    };

    // ComplexFormula ----------------------------------------------------------
    // Description: A class that represents Boolean formulae built by
    //              conjunction, disjunction and negation.
    // -------------------------------------------------------------------------
    var ComplexFormula = function(connective) {
        validateAsChar(connective);

        var myCachedString;

        if (!isOperator(connective))
            throw new Error('Not a valid connective: ' + connective);

        var myConnective = connective;

        // boolean NOT is unary; AND and OR are binary.
        var myArity = ('\'' === myConnective)? 1 : 2;

        if (1 + myArity !== arguments.length) {
            throw ('Wrong number of arguments for ' + myConnective);
        }

        var myFormulae = [];

        // have already ensured that arguments.length matches my arity
        for (var i = 1; i < arguments.length; i++) {
            validateAsFormula(arguments[i]);
            myFormulae.push(arguments[i]);
        }

        // toString ------------------------------------------------------------
        // Description: Public function that works by depth-first search.
        // Input:
        //     callStackDepth - For internal use only. Should be ignored by
        //                      client code so that the outermost call is
        //                      self-detected. Tldr, just call toString()!
        // ---------------------------------------------------------------------
        this.toString = function(callStackDepth) {

            // Keep track of the depth to avoid bracketing the outermost formula
            // The outermost call to toString should *not* provide a value!
            if ('number' === typeof(callStackDepth)) {
                callStackDepth++;
            } else {
                callStackDepth = 1;
            }

            // Keep a cached copy of my toString to avoid repeating the dfs.
            if (1 === callStackDepth && 'string' === typeof(myCachedString)) {
                return myCachedString;
            }

            var output = '';

            if (callStackDepth > 1 && 2 === myArity) output += '(';

            // start with my first argument
            output += myFormulae[0].toString(callStackDepth);
            // now my connective
            output += myConnective;
            // add the rest of my arguments, if I have any
            for (var i = 1; i < myArity; i++) {
                output += myFormulae[i].toString(callStackDepth);
            }

            if (callStackDepth > 1 && 2 === myArity) output += ')';

            myCachedString = output;

            return output;
        };

        // getArgument ---------------------------------------------------------
        // Description: accessor. Returns a copy.
        // ---------------------------------------------------------------------
        this.getArgument = function(idx) {
            return formulaFactory(myFormulae[idx].toString());
        };

        // More accessors ------------------------------------------------------
        this.getConnective = function() {
            return myConnective;
        };

        this.getArity = function() {
            return myArity;
        };
    };

    // equalFormulae -----------------------------------------------------------
    // Description: Tells whether its inputs are equal.
    // Inputs: f1, f2 Atoms, Constants or ComplexFormulas
    // Throws: Errors if f1 and f2 aren't as described above.
    // -------------------------------------------------------------------------
    var equalFormulae = function(f1, f2) {
       validateAsFormula(f1);
       validateAsFormula(f2);

       return f1.toString() === f2.toString();
    };

    // peek --------------------------------------------------------------------
    // Description: A little helper function to pretend a JS array is a stack
    //              with a peek() method.
    // -------------------------------------------------------------------------
    var peek = function (arr) {
        return arr[arr.length - 1];
    };

    // validateAsChar ----------------------------------------------------------
    // Description: Checks whether its input is a 1-length string.
    // Input: Should better be a 1-length string
    // Throws: Errors if input is anything else.
    // -------------------------------------------------------------------------
    var validateAsChar = function(input) {
        if (typeof(input) != 'string') {
            throw new Error('input not a string: ' + input);
        }

        if (1 !== input.length) {
            throw new Error ('input length incorrect: ' + input);
        }
    };

    // isOperator --------------------------------------------------------------
    // Description: Describes whether its input is an operator in Boolean
    // algebra.
    // Input: Should be a string one character long.
    // Returns: boolean
    // Throws: Descriptive error message if input is bogus.
    // -------------------------------------------------------------------------
    var isOperator = function(input) {

        return (operators.indexOf(input) !== -1);
    };

    // isOperand ---------------------------------------------------------------
    // Description: Describes whether its input is an operand in Boolean
    // algebra.
    // Input: Should be a string one character long.
    // Returns: bool
    // Throws: Descriptive error message if input is bogus.
    // -------------------------------------------------------------------------
    var isOperand = function(input) {
        return isBooleanConstantSymbol(input) ||
               isPropositionalAtomSymbol(input);
    };

    // isPropositionalAtomSymbol -----------------------------------------------
    // Description: Tells whether its input is a valid propositional atom.
    // Input: A 1-length string. Should be validated beforehand.
    // Returns: bool
    // Note: this is not configurable because YAGNI
    // -------------------------------------------------------------------------
    var isPropositionalAtomSymbol = function(input) {
    // ASCII values for a, z, A and Z characters.
        var a = 97;
        var z = 122;
        var A = 65;
        var Z = 90;

        var inputCode = input.charCodeAt(0);

        // Doing it this way is faster than the equivalent a-zA-Z regex.
        return (a <= inputCode && inputCode <= z) ||
               (A <= inputCode && inputCode <= Z);
    };

    // isBooleanConstantSymbol -------------------------------------------------
    // Descroption: States whether its input is either 1 or 0. Not configurable
    //              but could be made that way if anyone wanted to use BoolEdit
    //              for fancier logic.
    // -------------------------------------------------------------------------
    var isBooleanConstantSymbol = function(input) {
        return '1' === input || '0' === input;
    };

    // throwIfUnaryPrefix ------------------------------------------------------
    // Description: Throws an error if someone tries to sneak a unary operator
    //              through as a prefix. This is to handle a funky corner case
    //              that slips through Dijkstra's Shunting Yard, where 'p gets
    //              parsed into p' instead of throwing an error.
    // -------------------------------------------------------------------------
    var throwIfUnaryPrefix = function(infixString, idx) {
        if ("'" !== infixString[idx]) return;

        var errorMessage = 'no prefix negation';

        // ok: p'   p''     (p)'    ((p))'
        // not ok: 'p
        // if we walk back and find another ', a bracket or an operand, it's
        // all good, otherwise throw an error

        idx--;

        while (infixString[idx] === " ") idx--;

        if (idx < 0) throw new Error(errorMessage);

        var precedingChar = infixString[idx];

        if ( !( precedingChar === "'" ||
                precedingChar === ")" ||
                isOperand(precedingChar) ) ) {
            throw new Error(errorMessage);
        }
    };

    // toPostfix ---------------------------------------------------------------
    // Description: JS implementation of Dijkstra's Shunting Yard Algorithm.
    // Input: infox - a string representing a formula in infix notation
    // Returns: a formula in postfix notation
    // Throws: descriptive error strings if it encouters bad syntax or bogus
    //         input
    // -------------------------------------------------------------------------
    var toPostfix = function(infixString) {
        if ('string' !== typeof(infixString)) {
            throw ('input not a string: ' + infixString);
        }

        var output = '';
        var operatorStack = [];

        // I'm allowing some Unicode symbols e.g. '⊕'. This may result in some
        // strange results for the string.length property in older browsers.
        // tbd figure out whether there is a safer way of doing this or whether
        // it's even a big deal
        for (var i = 0, len = infixString.length; i < len; i++) {
            var currentChar = infixString[i];
            if (isOperand(currentChar)) {
                output += currentChar;
            } else if (isOperator(currentChar)) {

                throwIfUnaryPrefix(infixString, i);

                while (operatorStack.length &&
                       precedence[currentChar] <=
                       precedence[peek(operatorStack)]) {
                       // <= gives left associativity for my binary operators;
                       // < would give right
                    output += operatorStack.pop();
                }

                operatorStack.push(currentChar);
            } else if ('(' === currentChar) {
                operatorStack.push(currentChar);
            } else if (')' === currentChar) {
                while (operatorStack.length && peek(operatorStack) !== '(') {
                    output += operatorStack.pop();
                }

                if ('(' === peek(operatorStack))
                    operatorStack.pop(); // lose the left parenthesis
                else
                    throw new Error('missing left parenthesis');
            } else if (' ' === currentChar) {
                // Just ignore spaces; tabs and other forms of whitespace will
                // cause an error though
            } else {
                throw new Error('illegal character: ' + currentChar);
            }
        }

        while (operatorStack.length) {
            var nextOperator = operatorStack.pop();

            if ('(' === nextOperator)
                throw new Error('missing right parenthesis');
            else
                output += nextOperator;
        }

        return output;
    };

    // toFormula ---------------------------------------------------------------
    // Description: Converts a string in postfix notation to a valid formula
    //              (either an atom, a constant or a complex formuula)
    // Input : postfix - A string representing a formula in postfix notation.
    // Output : Atom/ Constant/ ComplexFormula, depending on input.
    // Throws: Errors if input syntax is bad.
    // -------------------------------------------------------------------------
    var toFormula = function(postfix) {

        var formulaStack = [];

        for (var i = 0, len = postfix.length; i < len; i++) {
            var thisChar = postfix[i];

            if (isPropositionalAtomSymbol(thisChar)) {
                formulaStack.push(new Atom(thisChar));
            } else if (isBooleanConstantSymbol(thisChar)) {
                formulaStack.push(new Constant(thisChar));
            } else if (isOperator(thisChar)) {
                var currentOperatorArity = ('\'' === thisChar)? 1 : 2;

                if (currentOperatorArity > formulaStack.length) {
                    throw new Error('missing operand');
                }

                if (currentOperatorArity === 1) {
                    formulaStack.push(
                        new ComplexFormula(
                            thisChar, formulaStack.pop()));
                } else if (currentOperatorArity === 2) {
                    var formula2 = formulaStack.pop();
                    var formula1 = formulaStack.pop();
                    formulaStack.push(
                        new ComplexFormula(
                            thisChar, formula1, formula2));
                } else {
                    throw new Error('Unidentified arity ' + arity);
                }
            } else {
                throw new Error('Invalid character: ' + thisChar);
            }
        }

        if (formulaStack.length !== 1) {
            throw new Error('missing operator');
        }

        return formulaStack.pop();
    };

    // Allow for elided and ( * )  symbols in user input as in pq
    var addElidedAnds = function(string) {
        string = string.replace(/ /g, '');

        var andIndices = findElidedAsteriskIndices(string);

        var offset = 0;

        for (var i = 0; i < andIndices.length; i++) {
            string = string.slice(0, andIndices[i] + offset) + '*' +
                     string.slice(andIndices[i] + offset);

            offset++;
        }

        return string;
    };

    // Returns the indexes of where you found symbols that could have omitted
    // ands between them
    var findElidedAsteriskIndices = function(string) {
        var idx = 0;

        var list = [];

        while (idx < string.length - 1) {
            var current = string[idx];
            var next = string[idx + 1];

            if (isAtomOrConstantOrRParenOrNot(current) &&
                isAtomOrConstantOrLParen(next)) {
                list.push(idx + 1);
            }

            idx++;
        }

        return list;
    };

    var isAtomOrConstantOrRParenOrNot = function(string) {
        return isAtomOrConstant(string) ||
               string === ')' ||
               string === "'";
    };

    var isAtomOrConstantOrLParen = function(string) {
        return isAtomOrConstant(string) || string === '(';
    };

    var isAtomOrConstant = function(string) {
        return isPropositionalAtomSymbol(string) ||
               isBooleanConstantSymbol(string);
    };

    var formulaFactory = function(infixString) {
        if ('string' !== typeof(infixString)) {
            throw new Error('not a string' + infixString);
        }

        infixString = addElidedAnds(infixString);
        var postfix = toPostfix(infixString);
        var formula = toFormula(postfix);

        return formula;
    };

    var operators = [];
    var precedence = {};

    var defineOperators = function(operatorDefinitionObject) {
        for (var key in operatorDefinitionObject) {
            if (operatorDefinitionObject.hasOwnProperty(key)) {
                operators.push(key);
                precedence[key] = operatorDefinitionObject[key].precedence;
            }
        }
    };

    /* test-code */
    myPublicInterface.__testonly__ = {};
    myPublicInterface.__testonly__.Atom = Atom;
    myPublicInterface.__testonly__.Constant = Constant;
    myPublicInterface.__testonly__.ComplexFormula = ComplexFormula;
    myPublicInterface.__testonly__.toPostfix = toPostfix;
    myPublicInterface.__testonly__.toFormula = toFormula;
    /* end-test-code */

    myPublicInterface.defineOperators = defineOperators;
    myPublicInterface.equalFormulae = equalFormulae;
    myPublicInterface.formulaFactory = formulaFactory;
    myPublicInterface.isBooleanConstantSymbol = isBooleanConstantSymbol;
    myPublicInterface.isPropositionalAtomSymbol = isPropositionalAtomSymbol;
    myPublicInterface.validateAsFormula = validateAsFormula;

    return myPublicInterface;

} ) (); // end revealing module BoolEdit
