// BoolEditProof ---------------------------------------------------------------
// Description: Module that presents an API for creating and editing proofs in
//              Boolean algebra. Intended to be JSON-friendly e.g. for loading
//              and saving Proofs to an external API.
// -----------------------------------------------------------------------------
var BoolEditProof = ( function() {

    // ProofLine ---------------------------------------------------------------
    // Description: Private inner class that provides a JSONic interface for
    //              creating lines of proofs. Each line consists of an entry
    //              formula, and a rule that may justify it.
    // Inputs: initializerObject - A bare JS object. Constructor pays attention
    //                  to two of its members:
    //                  rule - A string that should name a rule known by
    //                         BoolEditRules
    //                  formula - Output from BoolEditLanguage.formulaFactory
    // Throws: Errors for unknown / bogus rule name, plus throws errors from
    //         BoolEditLanguage.formulaFactory for bad syntax and other forms of
    //         bogusness
    // -------------------------------------------------------------------------
    var ProofLine = function(initializerObject) {

        // MEMBER VARIABLES

        // Pointer to the entry at this line
        var myFormula;
        // Pointer to the rule used at this line
        var myRule;
        // Boolean stating whether the entry at this line is justified by the
        // rule cited. Note that on its own, this line cannot determine whether
        // it's any good; this decision needs to be made by an outside entity
        // who can then use this flag to record its evaluation.
        var myEvaluation;
        var mySubMap;

        // CONSTRUCTION LOGIC

        myFormula = BoolEditLanguage.formulaFactory(initializerObject.formula);
        myRule = BoolEditRules.getRule(initializerObject.rule);

        // MEMBER FUNCTIONS

        // toJSObject ----------------------------------------------------------
        // Descripton: Creates a bare JS object that presents stringified
        //             information about this line.
        // Returns: see Description
        // ---------------------------------------------------------------------
        this.toJSObject = function() {
            var ruleName = this.getRuleString();

            return {
                formula: myFormula.toString(),
                rule: ruleName,
                isOk: myEvaluation,
                subMap: mySubMap
            };
        };

        // getEntryString ------------------------------------------------------
        // Description: Returns a stringified version of the entry at this line.
        // Returns: see Description.
        // ---------------------------------------------------------------------
        this.getEntryString = function() {
            return myFormula.toString();
        };

        // getRuleString -------------------------------------------------------
        // Description: Returns a string that names the rule justifying the
        //              entry at this line.
        // Returns: see Description.
        // ---------------------------------------------------------------------
        this.getRuleString = function() {
            var ruleName;

            if (undefined === myRule) {
                ruleName = undefined;
            } else {
                ruleName = myRule.getName();
            }

            return ruleName;
        };

        // setEvaluation -------------------------------------------------------
        // Description: Sets the eveluation of this line to true or false. Note
        //              that the evaluation set may not be true---care should
        //              be taken to ensure that only entities that know what
        //              they're doing call this function.
        // ---------------------------------------------------------------------
        this.setEvaluation = function(evaluation) {
            if (typeof(evaluation) !== "boolean") {
                throw new Error('non-boolean input: ' + evaluation);
            }

            myEvaluation = evaluation;
        };
    }; // end ProofLine

    var Proof = function(initializerObject) {

        // MEMBER VARIABLES

        // the left and right half of the equivalence that this proof is a
        // proof of
        var myLeft;
        var myRight;
        // all the lines of this proof
        var myLines = []; // end member variables

        // CONSTRUCTION LOGIC

        // validate input equivalence
        if (undefined === initializerObject ||
            undefined === initializerObject.equivalence) {
            throw new Error(
            'input to Proof constructor should be a JS object with a member called "equivalence"');
        }

        var equivalenceString = initializerObject.equivalence;


        if (typeof(equivalenceString) !== 'string') {
            throw new Error('non-string input:' + equivalenceString);
        }

        var splitArray = equivalenceString.split('=');

        if (splitArray.length !== 2) {
            throw new Error('bad syntax: ' + equivalenceString);
        }

        // No need to store fancy objects, just use strings
        myLeft = BoolEditLanguage.formulaFactory(splitArray[0]).toString();
        myRight = BoolEditLanguage.formulaFactory(splitArray[1]).toString();

        // copy over the lines that belong to the proof, if there are any
        if (undefined !== initializerObject.lines) {
            if (!(initializerObject.lines instanceof Array)) {
                throw new Error('non-array input: ' + initializerObject.lines);
            } else {
                for (var i = 0; i < initializerObject.lines.length; i++) {
                    // ProofLine constructor will handle validation
                    myLines.push(new ProofLine(initializerObject.lines[i]));
                }
            }
        } // end construction logic

        // MEMBER FUNCTIONS

        // addLine -------------------------------------------------------------
        // Description: Adds a line to this Proof.
        // Input: initializerObject - Bare JS object. This function pays
        //        attention to the following properties.
        //        lineNumber - should be a number. If none is specified the line
        //                     be added at the end of myLines.
        //        rule - A string that should name a rule known by
        //               BoolEditRules
        //        formula - Output from BoolEditLanguage.formulaFactory
        // Throws: errors if lineNumber is not a positive integer, rule doesn't
        //         name a rule known by BoolEditRules, or formula is invalid
        //         according to BoolEditLanguage.formulaFactory.
        // ---------------------------------------------------------------------
        this.addLine = function(initializerObject) {

            // ProofLine constructor validates rule and formula
            var lineToAdd = new ProofLine(initializerObject);

            var lineNumberIsSpecified =
                ( undefined !== initializerObject.lineNumber );

            if (lineNumberIsSpecified) { // do validation logic
                var inputLineNumber = initializerObject.lineNumber;
                if (inputLineNumber !== Math.floor(inputLineNumber) ||
                    inputLineNumber <= 0 ||
                    inputLineNumber > myLines.length + 1) {
                    throw new Error('bad line number: ' + inputLineNumber);
                } else {
                    // proofs begin numbering lines at 1
                    var index = inputLineNumber - 1;
                    myLines.splice(index, 0, lineToAdd);
                }
            } else {
                myLines.push(lineToAdd);
            }
        };

        // deleteLine ----------------------------------------------------------
        // Description: Deletes the line specified by idx. Assumes that lines
        //              are numbered beginning with 1.
        // Inputs: idx - The line number
        // ---------------------------------------------------------------------
        this.deleteLine = function(idx) {
            if ("number" === typeof(idx)) {
                if (idx <= myLines.length && idx > 0) {
                    var computerIdx = idx - 1; // mathematicians start numbering @ 1
                                               // but programmers start at 0
                    myLines.splice(computerIdx, 1);
                } else {
                    throw new Error("index out of range: " + idx);
                }
            } else {
                throw new Error("not a number: " + idx);
            }
        };

        // serEquivalence ------------------------------------------------------
        // Description: Sets the equivalence that belongs to this proof.
        // Input: string
        // Throws: Error if String isn't of the format A = B where A and B are
        //         valid formulae.
        // ---------------------------------------------------------------------
        this.setEquivalence = function(equivalenceString) {

            if (typeof(equivalenceString) !== 'string') {
                throw new Error('non-string input:' + equivalenceString);
            }

            var splitArray = equivalenceString.split('=');

            if (splitArray.length !== 2) {
                throw new Error('bad syntax: ' + equivalenceString);
            }

            // No need to store fancy objects, just use strings
            myLeft = BoolEditLanguage.formulaFactory(splitArray[0]).toString();
            myRight = BoolEditLanguage.formulaFactory(splitArray[1]).toString();

        };

        // firstLineStartsProof ------------------------------------------------
        // Description: Determines whether the first line of this proof states
        //              either the left or right side of the equivalence this
        //              proof is supposed to prove. Tags the first line with a
        //              boolean stating whether it's ok according to this
        //              metric; this is meant to be an aid to the user who wants
        //              to know where they went wrong.
        // returns: boolean
        // ---------------------------------------------------------------------
        var firstLineStartsProof = function() {
            var evaluation = false;

            // evaluate first line
            if (myLines.length > 0) {
                var firstLine = myLines[0];
                evaluation = (firstLine.getEntryString() === myLeft ||
                              firstLine.getEntryString() === myRight);

                firstLine.setEvaluation(evaluation);
            }

            return evaluation;
        };

        // allDerivationsOk ----------------------------------------------------
        // Description: States whether every line after the first line is
        //              justified by the rule given at that line Tags each line
        //              with a boolean stating on this basis whether it's OK.
        // Returns: boolean
        // ---------------------------------------------------------------------
        var allDerivationsOk = function () {
            var allDerivationsOk = true;

            for (var i = 1; i < myLines.length; i++) {
                var previousLine = BoolEditLanguage.formulaFactory(
                    myLines[i-1].getEntryString());
                var currentLine = BoolEditLanguage.formulaFactory(
                    myLines[i].getEntryString());
                var currentRule = BoolEditRules.getRule(
                    myLines[i].getRuleString());
                var currentEvaluation = (undefined !== currentRule)?
                    currentRule.doesJustify(
                        previousLine, currentLine).isTrue :
                    false;
                myLines[i].setEvaluation(currentEvaluation);

                allDerivationsOk = allDerivationsOk && currentEvaluation;
            }

            return allDerivationsOk;
        };

        // lastLineFinishesProof -----------------------------------------------
        // Description: States whether the last line of this proof presents
        //              whatever half of its equivalence isn't given in the
        //              first line.
        // Returns: boolean
        // ---------------------------------------------------------------------
        var lastLineFinishesProof = function() {
            if (myLines.length < 1) return false;

            var firstLine = myLines[0].getEntryString();
            var lastLine = myLines[myLines.length - 1].getEntryString();

            return (lastLine === myLeft && firstLine === myRight) ||
                   (lastLine === myRight && firstLine === myLeft);
        };

        // iAmComplete ---------------------------------------------------------
        // Description: Lets the caller know whether this proof is complete.
        //              The proof is complete iff:
        //              1. the first line states either the left or right side
        //                 of the equivalence this proof is supposed to prove
        //              2. every other line is justified by the rule given at
        //                 that line.
        //              3. the last line states whatever half of my equivalence
        //                 isn't stated in the first line.
        //              As a side effect, this function tags every line with a
        //              boolean stating whether it is acceptable. This will let
        //              the user know where they have gone wrong.
        // ---------------------------------------------------------------------
        var iAmComplete = function() {
            // We want the side effects of all three methods, so call them here
            // rather than as conjuncts of && to avoid short-circuiting
            var firstLineStartsProof_ = firstLineStartsProof();
            var allDerivationsOk_ = allDerivationsOk();
            var lastLineFinishesProof_ = lastLineFinishesProof();

            return firstLineStartsProof_ &&
                   allDerivationsOk_ &&
                   lastLineFinishesProof_;
        };

        // toJSObject ----------------------------------------------------------
        // Description: Returns a bare JS object suitable for JSON serialization
        //              that containts copies of all relevant information
        //              concerning this proof. That object can be used to feed
        //              the constructor and thereby copy this proof.
        // ---------------------------------------------------------------------
        this.toJSObject = function() {
            var output = {};

            output.equivalence = myLeft + ' = ' + myRight;
            output.isComplete = iAmComplete();
            output.lines = [];

            for (var i = 0; i < myLines.length; i++) {
                var currentLine = myLines[i].toJSObject();
                output.lines.push(currentLine);
            }

            return output;
        };
    }; // end Proof

    // This module only returns the Proof object; it's not a classic Revealing
    // Module, but my doing it this way serves to hide the ProofLine class
    // without jamming it into the definition of Proof.

    return Proof;

} )(); // end module BoolEditProof