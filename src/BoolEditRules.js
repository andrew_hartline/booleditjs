// BoolEditRules ---------------------------------------------------------------
// Description: Revealing module that exposes an API for creating and evaluating
//              proofs in Boolean algebra.
// -----------------------------------------------------------------------------
var BoolEditRules = ( function() {

    "use strict";

    var myPublicInterface = {};

    // isLeaf ------------------------------------------------------------------
    // Description: Determines whether its input should be considered a leaf
    //              in a tree traversal.
    // Input: formula - A Formula object.
    // Returns: true iff the Formula is an Atom or Constant.
    // -------------------------------------------------------------------------
    var isLeaf = function(formula) {
            return formula.getConnective === undefined;
    };

    // Rule --------------------------------------------------------------------
    // Description: Class to represent an equivalence rule in Boolean algebra.
    // Inputs: nameString - Will name the rule.
    //         leftString, rightString - Will form the left and right sides of
    //                                   the equivalence the rule presents.
    // -------------------------------------------------------------------------
    var Rule = function(nameString, leftString, rightString) {
        var myLeft = BoolEditLanguage.formulaFactory(leftString);
        var myRight = BoolEditLanguage.formulaFactory(rightString);
        var myName = nameString;

        this.getEquivalence = function() {
            return myLeft.toString() + ' = ' + myRight.toString();
        };

        this.getName = function() {
            return myName;
        };

        // doesJustifyWholeTreeInterderivability -------------------------------
        // Description: Private helper for doesJustify().
        //              Determines whether this rule justifies the
        //              interderivability of leftInput and rightInput,
        //              considered as whole trees.
        //              Does not drill, i.e. will show that a double negation
        //              rule will justify (p+q) = (p+q)'', but it is not smart
        //              enought to see that double negation means that
        //              (p+q) = (p''+q).
        //              Checks both directions.
        // Inputs: leftInput, rightInput - formula objects.
        // Returns: bool representing whole-tree interderivability
        // ---------------------------------------------------------------------
        var doesJustifyWholeTreeInterderivability = function(
            leftInput, rightInput) {
            var substitutionMap = {};

            // Try one direction...
            if ( uniformSubbable(myLeft, leftInput, substitutionMap) &&
                 uniformSubbable(myRight, rightInput, substitutionMap) ){
                return { isTrue: true, subMap: substitutionMap };
            }

                                  //  If you made it here,
            substitutionMap = {}; // clear out the failed substitutions...

            // ... and try the other direction.
            if ( uniformSubbable(myRight, leftInput, substitutionMap) &&
                 uniformSubbable(myLeft, rightInput, substitutionMap) ) {
                return { isTrue: true, subMap: substitutionMap };
            }

            // If you made it down here, just give up.
            return { isTrue: false };
        };

        // doesJustify ---------------------------------------------------------
        // Description: Returns true iff leftInput and rightInput are alike
        //              in every way except with respect to a single pair of
        //              branches whose whole-tree interderivablity is justified
        //              by this rule.
        // Inputs: leftInput, rightInput - formula objects.
        // Returns: bool representing whether this rule justifies the
        //          interderivability of leftInput and rightINput.
        // Throws: Errors if leftInput and rightInput aren't BoolEdit formula
        //         objects.
        // ---------------------------------------------------------------------
        this.doesJustify = function(leftInput, rightInput) {
            // this will throw errors if leftInput and rightInput aren't the
            // correct BoolEdit objects
            BoolEditLanguage.validateAsFormula(leftInput);
            BoolEditLanguage.validateAsFormula(rightInput);

            // First, check whether this rule justifies the difference between
            // the current pair of branches

            var result = doesJustifyWholeTreeInterderivability(
                leftInput, rightInput);

            if (result.isTrue) {
                return result;

            } else {

                var indices = getDifferentBranchIndexes(leftInput, rightInput);

                // Check whether the two inputs are exactly alike except for a
                // single different branch.
                // If so, we want to drill recursively into that pair of
                // different branches to see whether their difference is
                // justified by this rule.

                if (indices.length === 1) {
                    var singleDifferentBranchIdx = indices[0];

                    return this.doesJustify(
                        leftInput.getArgument(singleDifferentBranchIdx),
                        rightInput.getArgument(singleDifferentBranchIdx));
                }

                // If not, we now know that this rule doesn't justify the
                // derivation of rightInput from leftInput.

                return {isTrue: false};
            }
        };

        var getDifferentBranchIndexes = function(formula1, formula2) {
            if (isLeaf(formula1) || isLeaf(formula2)) {
                return [];
            }

            if (formula1.getConnective() !== formula2.getConnective()) {
                return [];
            }

            var differentBranchIndexes = [];

            var leftArity = formula1.getArity();

            for (var i = 0; i < leftArity; i++) {
                if ( !equalFormulae(formula1.getArgument(i),
                                    formula2.getArgument(i)) ) {
                    differentBranchIndexes.push(i);
                }
            }

            return differentBranchIndexes;
        };

    }; // end Rule;

    // uniformSubbable ---------------------------------------------------------
    // Answers the question: is destinationFormula obtainable from sourceFormula
    // by a series of uniform substitutions, given the substitution constraints
    // expressed in substitutionMap?
    // Inputs: sourceFormula  - the formula that the substitutions would operate
    //                          on
    //         destinationFormula  - the candidate product of substitutions
    //         substitutionMap - A mapping of atoms to formulae that they are
    //                           getting substituted for
    // Output: substitutionMap - A mapping of atoms to formulae that they are
    //                           getting substituted for. Can be used to
    //                           constrain further applications of this
    //                           function.
    // Returns: bool for yes/ no
    // -------------------------------------------------------------------------
    var uniformSubbable = function(sourceFormula,
                                   destinationFormula,
                                   substitutionMap) {
        if (isLeaf(sourceFormula)) {
            var leftChar = sourceFormula.toString();
            if (BoolEditLanguage.isPropositionalAtomSymbol(leftChar)) {
                if (substitutionMap.hasOwnProperty(leftChar)) {
                    var mappedValue = substitutionMap[leftChar];
                    return equalFormulae(mappedValue, destinationFormula);
                } else { // the current char is unmapped; map it
                    substitutionMap[leftChar] = destinationFormula;
                    return true;
                }
            } else if (BoolEditLanguage.isBooleanConstantSymbol(leftChar)) {
                return equalFormulae(sourceFormula, destinationFormula);
            } else {
                throw new Error('Unidentified leaf node: ' + leftChar);
            }
        } else { // i.e. we are at a branch, and must do more recursive drilling
            if (isLeaf(destinationFormula))
                return false;

            if (sourceFormula.getConnective() !==
                destinationFormula.getConnective()) {
                return false;
            } else { // check whether uniform subbability obtains for every
                     // parallell pair of subformulae
                for (var i = 0; i < sourceFormula.getArity(); i++) {
                    if (!uniformSubbable(sourceFormula.getArgument(i),
                                         destinationFormula.getArgument(i),
                                         substitutionMap)) {
                        return false;
                    }
                }

                return true;
            }
        }
    };

    // equalFormulae -----------------------------------------------------------
    // Description: Compares formulae by dfs-based toString method. This should
    //              be the only method used to compare Formulae as === alone
    //              will not produce expected results.
    // Inputs: left, right - Atoms, Constants or ComplexFormula objects
    // Returns: bool to represent equality.
    // -------------------------------------------------------------------------
    var equalFormulae = function(left, right) {
        return left.toString() === right.toString();
    };

    // RulesSet ----------------------------------------------------------------
    // Description: Object that functions as a container for Rules objects.
    //              Allows retrieval of rules by name. Only permits one rule
    //              per name; adding two rules with the same name will
    //              cause the first rule to be overwritten.
    // Input: rulesObject - A bare JS object whose properties are all of the
    //                      following form: key : 'A = B'
    // Throws : errors on bad value syntax
    // -------------------------------------------------------------------------
    var RulesSet = function(rulesObject) {
        var myRules = {};

        for (var key in rulesObject) {
            if (rulesObject.hasOwnProperty(key)) {
                var split = rulesObject[key].split('=');

                if (split.length !== 2) {
                    throw new Error('bad syntax');
                }

                // this will throw errors if the rule's formulae don't validate
                var currentRule = new Rule(key, split[0], split[1]);

                myRules[key] = currentRule;
            }
        }

        // getRule -------------------------------------------------------------
        // Description: Returns a Rule object by its name. Returns a copuy.
        // Inputs: name - string
        // Returns: Copy of the Rule that is named by name, or undefined if
        //          there is no rule here by that name.
        // ---------------------------------------------------------------------
        this.getRule = function(name) {
            var rulePointer = myRules[name];

            if (undefined !== rulePointer) {
                var equivalenceTerms = rulePointer.getEquivalence().split('=');
                return new Rule(rulePointer.getName(),
                                equivalenceTerms[0],
                                equivalenceTerms[1]);
            } else {
                return undefined;
            }
        };

        // getRulesList --------------------------------------------------------
        // Description: Returns an array of JS objects whose keys are 'rule' and
        //              'equivalence' for each of the rules in this RulesSet.
        //              Primarily for creating dropdown menus.
        // Returns: see Description.
        // ---------------------------------------------------------------------

        this.getRulesList = function() {
            var output = [];

            for (var ruleName in myRules) {
                if (myRules.hasOwnProperty(ruleName)) {
                    output.push({
                        name: ruleName,
                        equivalence: myRules[ruleName].getEquivalence()
                    });
                }
            }

            return output;
        };
    };

    // Private RulesSet belonging to this module only.
    var myRulesSet;

    // defineRules -------------------------------------------------------------
    // Description: Defines the rules belonging to BoolEditRules by passing in
    //              a single JSON object that contains definitions of all of
    //              them. Each description must be of the following form:
    //                  fooRuleName : A = B
    // -------------------------------------------------------------------------
    myPublicInterface.defineRules = function(rulesObject) {
        myRulesSet = new RulesSet(rulesObject);
    };

    // getRule -----------------------------------------------------------------
    // Description: Returns a Rule object by its name. Returns a copy.
    // Inputs: name - string
    // Returns: Copy of the Rule that is named by name, or undefined if there is
    //          no rule here by that name.
    // -------------------------------------------------------------------------
    myPublicInterface.getRule = function(name) {
        return myRulesSet.getRule(name);
    };

    // validateAsRule ----------------------------------------------------------
    // Description: Throws an error if its input is defined but not a BoolEdit
    //              rule (here, undefined counts as a null rule and will not
    //              throw an error).
    // Input: rule - Could be anything.
    // Throws: see description
    // -------------------------------------------------------------------------
    myPublicInterface.validateAsRule = function(rule) {
        if (!(rule instanceof Rule) && undefined !== rule) {
            throw new Error('not a BoolEdit rule: ' + rule);
        }
    };

    myPublicInterface.getRulesList = function() {
        return myRulesSet.getRulesList();

    };

    /* test-code */
    // note that all this will be removed by Grunt
    myPublicInterface.__testonly__ = {};
    myPublicInterface.__testonly__.Rule = Rule;
    myPublicInterface.__testonly__.uniformSubbable = uniformSubbable;
    /* end-test-code */

    return myPublicInterface;

} )(); // end module BoolEditRules