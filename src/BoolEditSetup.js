// BoolEditSetup.js ------------------------------------------------------------
// Description: A few lines of procedural code that configure BoolEditLanguage
//              and BoolEditProof modules using resources defined in
//              BoolEditConfig. Note that this configuration process could
//              easily be handled using remote services that send JSON objects
//              with the correct structure.
// -----------------------------------------------------------------------------

// Remember to define the language stuff before the rules, since the rules all
// pertain to sentences in a particular language.
BoolEditLanguage.defineOperators(BoolEditConfig.getOperatorsDefinition());

BoolEditRules.defineRules(BoolEditConfig.getRulesDefinition());

