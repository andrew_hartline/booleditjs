// export a single global for Meteor to use

BoolEdit = {
    Proof: BoolEditProof,
    Language: BoolEditLanguage,
    Rules: BoolEditRules,
};
