// Returns the last line of a proof. Doesn't affect the proof even though pop is
// used because toJSObject() returns a copy.
var lastLine = function(proof) {
    return proof.toJSObject().lines.pop();
};

describe("BoolEdit as configured for BCIT", function() {
    it ("should allow the first basic simplification from http://sandbox.mc.edu/~bennet/cs110/boolalg/simple.html",
        function() {
            var foo = new BoolEditProof({
                equivalence: "C + (B * C)' = 1"
            });
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({ formula: "C + (B * C)'"});
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "C + (B' + C')",
                rule: 'T15a'});
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "C + (C' + B')",
                rule: 'L6b'});
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(C + C') + B'",
                rule: 'L7b'});
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "1 + B'",
                rule: 'T12b'});
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "B' + 1",
                rule: 'L6b'});
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "1",
                rule: 'T10b'});
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(true);

        }
    );

    it ("should allow the second basic simplification from http://sandbox.mc.edu/~bennet/cs110/boolalg/simple.html",
        function() {
            var foo = new BoolEditProof({
                equivalence: "(A*B)' * (A' + B)*(B' + B) = A'"
            });
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A*B)' * (A' + B)*(B' + B) "
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A*B)' * (A' + B)*(B + B') ",
                rule: 'L6b'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A*B)' * (A' + B)*1 ",
                rule: 'T12b'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A*B)' * (A' + B) ",
                rule: 'T10a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A' + B') * (A' + B) ",
                rule: 'T15a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A' + (B' * B) ",
                rule: 'L8b'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A' + (B * B') ",
                rule: 'L6a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A' + 0 ",
                rule: 'T12a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A'",
                rule: 'T9b'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(true);

        }
    );

    it ("should allow the third basic simplification from http://sandbox.mc.edu/~bennet/cs110/boolalg/simple.html",
        function() {
            var foo = new BoolEditProof({
                equivalence: "(A + C) * (A*D + A*D') + A*C + C = A + C"
            });
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + C) * (A*D + A*D') + A*C + C "
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + C) * (A*(D + D')) + A*C + C ",
                rule: 'L8a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + C) * (A*1) + A*C + C ",
                rule: 'T12b'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + C) * A + A*C + C ",
                rule: 'T10a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A * (A + C) + A*C + C ",
                rule: 'L6a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A  * ((A + C) + C) + C ",
                rule: 'L8a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A  * (A + (C + C)) + C ",
                rule: 'L7b'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A  * (A + C) + C ",
                rule: 'T11b'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A * A + A * C) + C ",
                rule: 'L8a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + A * C) + C ",
                rule: 'T11a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + C ",
                rule: 'T14a'
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(true);
        }
    );

    it ("should allow the fourth basic simplification from http://sandbox.mc.edu/~bennet/cs110/boolalg/simple.html",
        function() {
            var foo = new BoolEditProof({
                equivalence: "A * (A + B) + (B + A * A) * (A + B) = A + B"
            });
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A * (A + B) + (B + A * A) * (A + B)"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A * (A + B) + (B + A) * (A + B)",
                rule: "T11a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A * A + A * B) + (B + A) * (A + B)",
                rule: "L8a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + A * B) + (B + A) * (A + B)",
                rule: "T11a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + (B + A) * (A + B)",
                rule: "T14a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + (B + A) * (B + A)",
                rule: "L6b"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + (B + A)",
                rule: "T11a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + (A + B)",
                rule: "L6b"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + A) + B",
                rule: "L7b"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + B",
                rule: "T11b"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(true);
        }
    );

    it ("should allow the fourth basic simplification from http://sandbox.mc.edu/~bennet/cs110/boolalg/simple.html, TURBO BACKWARDS EDITION",
        function() {
            var foo = new BoolEditProof({
                equivalence: "A * (A + B) + (B + A * A) * (A + B) = A + B"
            });
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + B"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + A) + B",
                rule: "T11b"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + (A + B)",
                rule: "L7b"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + (B + A)",
                rule: "L6b"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + (B + A) * (B + A)",
                rule: "T11a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A + (B + A) * (A + B)",
                rule: "L6b"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A + A * B) + (B + A) * (A + B)",
                rule: "T14a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "(A * A + A * B) + (B + A) * (A + B)",
                rule: "T11a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A * (A + B) + (B + A) * (A + B)",
                rule: "L8a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(false);

            foo.addLine({
                formula: "A * (A + B) + (B + A * A) * (A + B)",
                rule: "T11a"
            });
            expect(lastLine(foo).isOk).toEqual(true);
            expect(foo.toJSObject().isComplete).toEqual(true);
        }
    );
});