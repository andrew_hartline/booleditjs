describe("BoolEditLanguage", function() {



    it('should not let Atom or Constant constructors be empty', function() {

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom();
        }).toThrowError("input not a string: undefined");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant();
        }).toThrowError("input not a string: undefined");

    });

    it('should not let Atom or Constant constructors take arrays', function() {

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom([]);
        }).toThrowError("input not a string: ");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant([]);
        }).toThrowError("input not a string: ");

    });

    it('should not let Atom or Constant constructors take objects', function() {

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom({});
        }).toThrowError("input not a string: [object Object]");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant({});
        }).toThrowError("input not a string: [object Object]");

    });

    it('should not let Atom or Constant constructors take numbers', function() {

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom(1);
        }).toThrowError("input not a string: 1");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant(0);
        }).toThrowError("input not a string: 0");

    });

    it('should not let Atom constructors take non-letters', function() {

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom("-");
        }).toThrowError("Not a valid propositional atom: -");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom("*");
        }).toThrowError("Not a valid propositional atom: *");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom("2");
        }).toThrowError("Not a valid propositional atom: 2");
    });

     it('should not let Constant constructors take chars other than 1 or 0',
        function() {

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant("-");
        }).toThrowError("Not a valid Boolean constant: -");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant("p");
        }).toThrowError("Not a valid Boolean constant: p");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant("2");
        }).toThrowError("Not a valid Boolean constant: 2");
    });

    it('should not let Atom or Constant constructors take strings of length !== 1',
        function() {

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom("ass");
        }).toThrowError("input length incorrect: ass");


        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Atom("");
        }).toThrowError("input length incorrect: ");

        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant("ass");
        }).toThrowError("input length incorrect: ass");


        expect(function() {
            var foo = new BoolEditLanguage.__testonly__.Constant("");
        }).toThrowError("input length incorrect: ");

    });

    it('should let Atom constructors take a-z and A-Z', function() {

        expect(new BoolEditLanguage.__testonly__.Atom('a').toString()).toEqual("a");
        expect(new BoolEditLanguage.__testonly__.Atom('z').toString()).toEqual("z");
        expect(new BoolEditLanguage.__testonly__.Atom('A').toString()).toEqual("A");
        expect(new BoolEditLanguage.__testonly__.Atom('Z').toString()).toEqual("Z");
    });

    it('should let Constant constructors take 1 and 0', function() {

        expect(new BoolEditLanguage.__testonly__.Constant('1').toString()).toEqual("1");
        expect(new BoolEditLanguage.__testonly__.Constant('0').toString()).toEqual("0");
    });

    it('should let ComplexFormula constructors make negated atoms', function() {
        var foo = new BoolEditLanguage.__testonly__.ComplexFormula('\'', new BoolEditLanguage.__testonly__.Atom('p'));
        expect(foo.toString()).toEqual("p'");
    });

    it('should let ComplexFormula constructors make arbitrarily negated formulae', function() {
        var foo = new BoolEditLanguage.__testonly__.ComplexFormula('\'', new BoolEditLanguage.__testonly__.Atom('p'));

        for (var i = 0; i < 9; i++) {
            foo = new BoolEditLanguage.__testonly__.ComplexFormula('\'', foo);
        }

        expect(foo.toString()).toEqual("p''''''''''");
    });

    it('should let ComplexFormula constructors make conjunctions and disjunctions out of atoms', function() {
        var foo = new BoolEditLanguage.__testonly__.Atom('p');
        var bar = new BoolEditLanguage.__testonly__.Atom('q');

        var baz = new BoolEditLanguage.__testonly__.ComplexFormula('*', foo, bar);
        var bat = new BoolEditLanguage.__testonly__.ComplexFormula('+', foo, bar);

        expect(baz.toString()).toEqual('p*q');
        expect(bat.toString()).toEqual('p+q');
    });

    it('should let ComplexFormula constructors make conjunctions and disjunctions out of atoms + complex formulae', function() {
        var foo = new BoolEditLanguage.__testonly__.Atom('p');
        var bar = new BoolEditLanguage.__testonly__.Atom('q');

        var baz = new BoolEditLanguage.__testonly__.ComplexFormula('*', foo, bar);

        var fizz = new BoolEditLanguage.__testonly__.ComplexFormula('+', foo, baz);
        var buzz = new BoolEditLanguage.__testonly__.ComplexFormula('*', baz, fizz);

        expect(fizz.toString()).toEqual('p+(p*q)');
        expect(buzz.toString()).toEqual('(p*q)*(p+(p*q))');
    });

    it('should correctly convert p+q to pq+', function() {

        var foo = BoolEditLanguage.__testonly__.toPostfix('p+q');
        expect(foo).toEqual('pq+');
    });

    it('should correctly convert p*q to pq*', function() {

        var foo = BoolEditLanguage.__testonly__.toPostfix('p*q');
        expect(foo).toEqual('pq*');
    });


    it("should correctly parse p'", function() {

        var foo = BoolEditLanguage.formulaFactory("p'");
        expect(foo.toString()).toEqual("p'");
    });

    it("should correctly parse pq", function() {

        var foo = BoolEditLanguage.formulaFactory("   p   q");
        expect(foo.toString()).toEqual("p*q");
    });

    it("should correctly parse p(q+r)", function() {

        var foo = BoolEditLanguage.formulaFactory("pq");
        expect(foo.toString()).toEqual("p*q");
    });

    it("should correctly parse (p + q)(q+r)", function() {

        var foo = BoolEditLanguage.formulaFactory("(p + q)(q+r)");
        expect(foo.toString()).toEqual("(p+q)*(q+r)");
    });

    it("should correctly parse (p + q)(q+r)", function() {

        var foo = BoolEditLanguage.formulaFactory("(p1)(q0)");
        expect(foo.toString()).toEqual("(p*1)*(q*0)");
    });

    it("should correctly parse p'q", function() {

        var foo = BoolEditLanguage.formulaFactory("p'q");
        expect(foo.toString()).toEqual("p'*q");
    });

    it("should correctly parse p'''(q''r')'", function() {

        var foo = BoolEditLanguage.formulaFactory("p'''(q''r')'");
        expect(foo.toString()).toEqual("p'''*(q''*r')'");
    });

    it('should correctly parse p+q', function() {

        var foo = BoolEditLanguage.formulaFactory('p+q');
        expect(foo.toString()).toEqual('p+q');
    });

    it('should correctly parse p*q', function() {

        var foo = BoolEditLanguage.formulaFactory('p*q');
        expect(foo.toString()).toEqual('p*q');
    });

    it("should correctly parse p'+q", function() {

        var foo = BoolEditLanguage.formulaFactory("p'+q");
        expect(foo.toString()).toEqual("p'+q");
    });


    it('should parse binary operators as left-associative', function() {

        var foo = BoolEditLanguage.formulaFactory("A+B+C+D+E+F+G");
        var bar = BoolEditLanguage.formulaFactory("H*I*J*K*L*M*N*O*P");

        expect(foo.toString()).toEqual("(((((A+B)+C)+D)+E)+F)+G");
        expect(bar.toString()).toEqual("(((((((H*I)*J)*K)*L)*M)*N)*O)*P");
    });

    it('should let parentheses defeat precedence rules', function() {

        var foo = BoolEditLanguage.formulaFactory("((A+B)*C)'");

        expect(foo.toString()).toEqual("((A+B)*C)'");


    });

    it("should correctly parse f'+o''*0'''+b''''*a'''''+r'''''', combining correct operator precedence with left associativity", function() {

        var foo = BoolEditLanguage.formulaFactory("f'+o''*0'''+b''''*a'''''+r''''''");
        expect(foo.toString()).toEqual("((f'+(o''*0'''))+(b''''*a'''''))+r''''''");

    });

    it("should ignore whitespace when parsing", function() {

        var foo = BoolEditLanguage.formulaFactory("       f       '+o'       '*0''       '+b       ''''*a''       ''       '+r'       ''       ''       '");
        expect(foo.toString()).toEqual("((f'+(o''*0'''))+(b''''*a'''''))+r''''''");

        var bar = BoolEditLanguage.formulaFactory("                    a               ");
    });

    it("should notice missing parentheses when parsing", function() {

        expect(function() {
            var foo = BoolEditLanguage.formulaFactory("((f'+(o''*0''')+(b''''*a'''''))+r''''''");
        }).toThrowError("missing right parenthesis");


        expect(function() {
            var foo = BoolEditLanguage.formulaFactory("((((((((((((((((((((p))))))))))))))))))");
        }).toThrowError("missing right parenthesis");

        expect(function() {
            var foo = BoolEditLanguage.formulaFactory("(((((((((((((((((((p))))))))))))))))))))");
        }).toThrowError("missing left parenthesis");

    });

    it('should notice crossed parentheses when parsing', function() {
        expect(function() {
            var foo = BoolEditLanguage.formulaFactory("))<<>>((");
        }).toThrowError("missing left parenthesis");
    });

    it('should not allow prefix negation outside a simple formula', function() {
        expect(function() {
            var foo = BoolEditLanguage.formulaFactory("'a");
        }).toThrowError("no prefix negation");
    });

    it('should not allow prefix negation within a complex formula',
       function() {
            expect(function() {
                var foo = BoolEditLanguage.formulaFactory("P + '(a * b)");
            }).toThrowError("no prefix negation");

            expect(function() {
                var foo = BoolEditLanguage.formulaFactory("'(p + q) + q'");
            }).toThrowError("no prefix negation");
    });

    it('should not allow prefix negation outside a complex formula',
       function() {
            expect(function() {
                var foo = BoolEditLanguage.formulaFactory("''(P + (a * b))");
            }).toThrowError("no prefix negation");
    });
});