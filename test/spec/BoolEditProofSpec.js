    // Returns the last line of a proof. Doesn't affect the proof even though pop is
// used because toJSObject() returns a copy.
var lastLine = function(proof) {
    return proof.toJSObject().lines.pop();
};

describe("BoolEditProof", function() {
    it('should give a useful error message when someone messes up the construction of a proof', function() {
        expect(function() {
            var proof = new BoolEditProof();
        }).toThrowError('input to Proof constructor should be a JS object with a member called "equivalence"');
    });

    it('should know its equivalence', function() {

        var proof = new BoolEditProof({
            equivalence: 'p=p'
        });

        expect(proof.toJSObject().equivalence).toEqual('p = p');
    });

    it('should allow the construction of a proof and the addition of a line',
        function() {
        var proof = createProof();

        proof.addLine({formula: 'p'});

        expect(proof.toJSObject().lines.length).toEqual(1);
        expect(proof.toJSObject().lines[0].formula).toEqual('p');

    });

    var createProof = function() {
        var proof = new BoolEditProof({
            equivalence: "p=p'"
        });

        return proof;
    }

    it ("should allow the resetting of a proof's equivalence", function() {
        var proof = new BoolEditProof({
            equivalence: 'p = q'
        });

        proof.setEquivalence('r = s');

        expect(proof.toJSObject().equivalence).toEqual('r = s');
    });

    it ("should disallow the resetting of a proof's equivalence when the second formula is bad", function() {
        var proof = new BoolEditProof({
            equivalence: 'p = q'
        });

        expect(function() {
            proof.setEquivalence('a = alksdjfla;sdkjf');
        }).toThrowError("illegal character: ;");
    });

    it ("should disallow the resetting of a proof's equivalence when the second formula is nonexistent", function() {
        var proof = new BoolEditProof({
            equivalence: 'p = q'
        });

        expect(function() {
            proof.setEquivalence('a = ');
        }).toThrow();
    });

    it ("should disallow the resetting of a proof's equivalence when the first formula is bad", function() {
        var proof = new BoolEditProof({
            equivalence: 'p = q'
        });

        expect(function() {
            proof.setEquivalence('aldkfjasl;dk = g');
        }).toThrow();
    });

    it ("should disallow the resetting of a proof's equivalence when the first formula is nonexistent", function() {
        var proof = new BoolEditProof({
            equivalence: 'p = q'
        });

        expect(function() {
            proof.setEquivalence(' = a');
        }).toThrow();
    });

    it ("should disallow the resetting of a proof's equivalence when there are too many things equivocated", function() {
        var proof = new BoolEditProof({
            equivalence: 'p = q'
        });

        expect(function() {
            proof.setEquivalence('p = q = ha ha hax');
        }).toThrow();
    });

    it ("should disallow the resetting of a proof's equivalence when only one thing is there", function() {
        var proof = new BoolEditProof({
            equivalence: 'p = q'
        });

        expect(function() {
            proof.setEquivalence('p');
        }).toThrow();
    });


    it('should detect bogus first lines', function() {

        var proof = new BoolEditProof({
            equivalence: "p+q=p''+q"
        });

        proof.addLine({formula: 'f + 0 + 0'});

        expect(proof.toJSObject().isComplete).toEqual(false);
    });

    it('should deny that a proof containing a bad line is complete', function() {

        var proof = new BoolEditProof({
            equivalence: "p+q=p''+q"
        });

        proof.addLine({formula: 'p + q'});
        proof.addLine({formula: "p'' + q", rule: 'T11a'});

        expect(proof.toJSObject().isComplete).toEqual(false);
    });

    it('should allow the construction of a proof and the addition of a couple of lines that make the proof go from incomplete to complete', function() {

        var proof = new BoolEditProof({
            equivalence: "p+q=p''+q"
        });

        proof.addLine({formula: 'p + q'});

        expect(proof.toJSObject().isComplete).toEqual(false);

        proof.addLine({formula: "p'' + q", rule: 'T13a'});

        expect(proof.toJSObject().isComplete).toEqual(true);
    });

    it('should allow the user to complete a proof in either a leftward or rightward direction', function() {

        var proof = new BoolEditProof({
            equivalence: "p+q = p''+q"
        });

        proof.addLine({formula: 'p + q'});

        expect(proof.toJSObject().isComplete).toEqual(false);

        proof.addLine({formula: "p'' + q", rule: 'T13a'});

        expect(proof.toJSObject().isComplete).toEqual(true);

        var bar = new BoolEditProof({
            equivalence: "p+q = p''+q"
        });

        bar.addLine({formula: "p'' + q"});

        expect(bar.toJSObject().isComplete).toEqual(false);

        bar.addLine({formula: "p + q",
                     rule: 'T13a'});

        expect(bar.toJSObject().isComplete).toEqual(true);

    });

    it('should notice that lines with null justifications after line 1 are not OK', function() {

        var proof = new BoolEditProof({
            equivalence: "p+q = p''+q"
        });

        proof.addLine({formula: 'p + q'});
        proof.addLine({formula: "p'' + q"});

        expect(proof.toJSObject().lines[0].rule).toEqual(undefined);
        expect(proof.toJSObject().lines[0].isOk).toEqual(true);
        expect(proof.toJSObject().lines[1].rule).toEqual(undefined);
        expect(proof.toJSObject().lines[1].isOk).toEqual(false);
    });


    it ("should allow the deletion of lines by index",
        function() {
            var proof = new BoolEditProof({
                equivalence: "A * (A + B) + (B + A * A) * (A + B) = A + B"
            });
            expect(proof.toJSObject().isComplete).toEqual(false);

            proof.addLine({
                formula: "A + B"
            });

            proof.addLine({
                formula: "(A + A) + B",
                rule: "T11b"
            });

            proof.addLine({
                formula: "A + (A + B)",
                rule: "L7b"
            });

            proof.addLine({
                formula: "A + (B + A)",
                rule: "L6b"
            });

            proof.addLine({
                formula: "A + (B + A) * (B + A)",
                rule: "T11a"
            });

            proof.addLine({
                formula: "A + (B + A) * (A + B)",
                rule: "L6b"
            });

            proof.addLine({
                formula: "(A + A * B) + (B + A) * (A + B)",
                rule: "T14a"
            });

            proof.addLine({
                formula: "(A * A + A * B) + (B + A) * (A + B)",
                rule: "T11a"
            });

            proof.addLine({
                formula: "A * (A + B) + (B + A) * (A + B)",
                rule: "L8a"
            });

            proof.addLine({
                formula: "A * (A + B) + (B + A * A) * (A + B)",
                rule: "T11a"
            });

            expect(lastLine(proof).isOk).toEqual(true);
            expect(proof.toJSObject().isComplete).toEqual(true);
            expect(proof.toJSObject().lines.length).toEqual(10);
            expect(proof.toJSObject().lines[0].formula).toEqual('A+B');

            proof.deleteLine(1);
            expect(proof.toJSObject().isComplete).toEqual(false);
            expect(proof.toJSObject().lines.length).toEqual(9);
            expect(proof.toJSObject().lines[0].formula).toEqual('(A+A)+B');

            expect(lastLine(proof).formula).toEqual("(A*(A+B))+((B+(A*A))*(A+B))");
            proof.deleteLine(proof.toJSObject().lines.length);

            expect(lastLine(proof).formula).toEqual("(A*(A+B))+((B+A)*(A+B))");

            expect(function() {
               proof.deleteLine(0);
            }).toThrowError("index out of range: 0");

            var bogusIdx = proof.toJSObject().lines.length + 1;

            expect(function() {
                proof.deleteLine(bogusIdx);
            }).toThrowError("index out of range: " + bogusIdx);

            expect(function() {
                proof.deleteLine([]);
            }).toThrowError("not a number: " + []);

            expect(function() {
                proof.deleteLine({});
            }).toThrowError("not a number: " + {});

            expect(function() {
                proof.deleteLine("fopoaijefoai");
            }).toThrowError("not a number: " + "fopoaijefoai");

            expect(function() {
                proof.deleteLine("1");
            }).toThrowError("not a number: " + "1");
        }
    );

    it ("should unserialize a serialized proof in a way that makes it identical with its original",
        function() {
            var proof = new BoolEditProof({
                equivalence: "A * (A + B) + (B + A * A) * (A + B) = A + B"
            });
            expect(proof.toJSObject().isComplete).toEqual(false);

            proof.addLine({
                formula: "A + B"
            });

            proof.addLine({
                formula: "(A + A) + B",
                rule: "T11b"
            });

            proof.addLine({
                formula: "A + (A + B)",
                rule: "L7b"
            });

            proof.addLine({
                formula: "A + (B + A)",
                rule: "L6b"
            });

            proof.addLine({
                formula: "A + (B + A) * (B + A)",
                rule: "T11a"
            });

            proof.addLine({
                formula: "A + (B + A) * (A + B)",
                rule: "L6b"
            });

            proof.addLine({
                formula: "(A + A * B) + (B + A) * (A + B)",
                rule: "T14a"
            });

            proof.addLine({
                formula: "(A * A + A * B) + (B + A) * (A + B)",
                rule: "T11a"
            });

            proof.addLine({
                formula: "A * (A + B) + (B + A) * (A + B)",
                rule: "L8a"
            });

            proof.addLine({
                formula: "A * (A + B) + (B + A * A) * (A + B)",
                rule: "T11a"
            });

            var bar = new BoolEditProof(proof.toJSObject());

            expect(bar.toJSObject()).toEqual(proof.toJSObject());
        }
    );

    it ("should allow you to insert lines at user-specified indices ",
        function() {
            var proof = new BoolEditProof({
                equivalence: "A * (A + B) + (B + A * A) * (A + B) = A + B"
            });

            proof.addLine({
                formula: "A + B"
            });

            proof.addLine({
                formula: "A + (A + B)",
                rule: "L7b"
            });

            proof.addLine({
                formula: "(A + A) + B",
                rule: "T11b",
                lineNumber: 2
            });

            expect(proof.toJSObject().lines[1].formula).toEqual("(A+A)+B");
            expect(proof.toJSObject().lines[2].formula).toEqual("A+(A+B)");

            proof.addLine({
                formula: "(F + o + o)",
                rule: "T11b",
                lineNumber: 1
            });

            expect(proof.toJSObject().lines[0].formula).toEqual("(F+o)+o");
        }
    );

    it ("should allow you to insert lines at the very last index and not throw",
        function() {
            var proof = new BoolEditProof({
                equivalence: "A * (A + B) + (B + A * A) * (A + B) = A + B"
            });

            proof.addLine({
                formula: "A + B"
            });

            proof.addLine({
                formula: "A + (A + B)",
                rule: "L7b"
            });


            expect(
                function() {
                    proof.addLine({
                        formula: "(F + o + o)",
                        rule: "T11b",
                        lineNumber: 3
                    });
                }
            ).not.toThrow();

            expect(proof.toJSObject().lines[2].formula).toEqual("(F+o)+o");
        }
    );

    it ("should throw when you insert a line at a too-big index ",
        function() {
            var proof = new BoolEditProof({
                equivalence: "A * (A + B) + (B + A * A) * (A + B) = A + B"
            });

            proof.addLine({
                formula: "A + B"
            });

            proof.addLine({
                formula: "A + (A + B)",
                rule: "L7b"
            });


            expect(
                function() {
                    proof.addLine({
                        formula: "(F + o + o)",
                        rule: "T11b",
                        lineNumber: 4
                    });
                }
            ).toThrow();

            expect(
                function() {
                    proof.addLine({
                        formula: "(F + o + o)",
                        rule: "T11b",
                        lineNumber: 7
                    });
                }
            ).toThrow();

            expect(
                function() {
                    proof.addLine({
                        formula: "(F + o + o)",
                        rule: "T11b",
                        lineNumber: 7
                    });
                }
            ).toThrow();

            expect(
                function() {
                    proof.addLine({
                        formula: "(F + o + o)",
                        rule: "T11b",
                        lineNumber: -1
                    });
                }
            ).toThrow();

            expect(
                function() {
                    proof.addLine({
                        formula: "(F + o + o)",
                        rule: "T11b",
                        lineNumber: 2.5
                    });
                }
            ).toThrow();

            expect(
                function() {
                    proof.addLine({
                        formula: "(F + o + o)",
                        rule: "T11b",
                        lineNumber: "no"
                    });
                }
            ).toThrow();
        }
    );

    it ("should show that a line derived from a bad line correctly is good ",
        function() {
            var proof = new BoolEditProof({
                equivalence: "A = A + A"
            });

            proof.addLine({
                formula: "B"
            });

            proof.addLine({
                formula: "(B + B)",
                rule: "T11b"
            });

            expect(proof.toJSObject().lines[1].isOk).toEqual(true);
        }
    );
});