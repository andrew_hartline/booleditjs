describe("BoolEditRules", function() {
    it('should indicate that p+q is usubbable from p', function() {

        var atom = BoolEditLanguage.formulaFactory('p');
        var formula = BoolEditLanguage.formulaFactory('p+q');
        var map = {};

        expect(BoolEditRules.__testonly__.uniformSubbable(atom, formula, map)).toEqual(true);
    });

    it("should indicate that p+q is not usubbable from p'", function() {

        var atom = BoolEditLanguage.formulaFactory('p');
        var formula = BoolEditLanguage.formulaFactory('p+q');
        var map = {};

        expect(BoolEditRules.__testonly__.uniformSubbable(atom, formula, map)).toEqual(true);
    });


    it("should indicate that f+o*0+b'''*a+r is usubbable from p", function() {

        var atom = BoolEditLanguage.formulaFactory('p');
        var formula = BoolEditLanguage.formulaFactory("f+o*0+b*a+r");
        var map = {};

        expect(BoolEditRules.__testonly__.uniformSubbable(atom, formula, map)).toEqual(true);
    });

    it("should indicate that f+o+0 is not usubbable from p * q ", function() {

        var foo = BoolEditLanguage.formulaFactory('p * q');
        var bar = BoolEditLanguage.formulaFactory("f+o+0");
        var map = {};

        expect(BoolEditRules.__testonly__.uniformSubbable(foo, bar, map)).toEqual(false);
    });

    it("should allow basic retrieval of rules by name", function() {
        expect(BoolEditRules.getRule('T13b').getEquivalence()).toEqual("x = x''");
    });

    it("should allow retrieval of a list of rule names", function() {
        expect(BoolEditRules.getRulesList()).toEqual( [ Object({ name: 'P2a', equivalence: '0*0 = 0' }), Object({ name: 'P2b', equivalence: '0+0 = 0' }), Object({ name: 'P3a', equivalence: '1*1 = 1' }), Object({ name: 'P3b', equivalence: '1+1 = 1' }), Object({ name: 'P4a', equivalence: '1*0 = 0' }), Object({ name: 'P4b', equivalence: '1+0 = 1' }), Object({ name: 'P5a', equivalence: "1' = 0" }), Object({ name: 'P5b', equivalence: "0' = 1" }), Object({ name: 'L6a', equivalence: 'x*y = y*x' }), Object({ name: 'L6b', equivalence: 'x+y = y+x' }), Object({ name: 'L7a', equivalence: 'x*(y*z) = (x*y)*z' }), Object({ name: 'L7b', equivalence: 'x+(y+z) = (x+y)+z' }), Object({ name: 'L8a', equivalence: 'x*(y+z) = (x*y)+(x*z)' }), Object({ name: 'L8b', equivalence: 'x+(y*z) = (x+y)*(x+z)' }), Object({ name: 'T9a', equivalence: 'x*0 = 0' }), Object({ name: 'T9b', equivalence: 'x+0 = x' }), Object({ name: 'T10a', equivalence: 'x*1 = x' }), Object({ name: 'T10b', equivalence: 'x+1 = 1' }), Object({ name: 'T11a', equivalence: 'x*x = x' }), Object({ name: 'T11b', equivalence: 'x+x = x' }), Object({ name: 'T12a', equivalence: "x*x' = 0" }), Object({ name: 'T12b', equivalence: "x+x' = 1" }), Object({ name: 'T13a', equivalence: "x'' = x" }), Object({ name: 'T13b', equivalence: "x = x''" }), Object({ name: 'T14a', equivalence: 'x+(x*y) = x' }), Object({ name: 'T14b', equivalence: 'x*(x+y) = x' }), Object({ name: 'T14c', equivalence: "x*(x'+y) = x*y" }), Object({ name: 'T14d', equivalence: "x+(x'*y) = x+y" }), Object({ name: 'T15a', equivalence: "(x*y)' = x'+y'" }), Object({ name: 'T15b', equivalence: "(x+y)' = x'*y'" }), Object({ name: 'df. ⊕', equivalence: "x⊕y = (x*y')+(x'*y)" }), Object({ name: 'df. ⊙', equivalence: "x⊙y = (x*y)+(x'*y')" }), Object({ name: 'df. ↑', equivalence: "x↑y = (x*y)'" }), Object({ name: 'df. ↓', equivalence: "x↓y = (x+y)'" }) ]);
    });

    it('should let double negation work rightwards', function() {
        var foo = BoolEditLanguage.formulaFactory('p');
        var bar = BoolEditLanguage.formulaFactory("p''");

        expect
            (BoolEditRules.getRule('T13b').doesJustify(foo, bar).isTrue)
            .toBe(true);
    });

    it('should let double negation work leftwards', function() {
        var foo = BoolEditLanguage.formulaFactory('p');
        var bar = BoolEditLanguage.formulaFactory("p''");

        expect(BoolEditRules.getRule('T13b').doesJustify(bar, foo).isTrue)
            .toBe(true);
    });

    it('should let double negation  work in a basic negative case', function() {
        var foo = BoolEditLanguage.formulaFactory('p');
        var bar = BoolEditLanguage.formulaFactory('q');

        expect(BoolEditRules.getRule('T13b').doesJustify(foo, bar).isTrue)
            .toBe(false);
    });

    it('should let double negation work rightwards on an embedded formula', function() {
        var foo = BoolEditLanguage.formulaFactory('p + q');
        var bar = BoolEditLanguage.formulaFactory("p'' + q");

        expect(BoolEditRules.getRule('T13b').doesJustify(foo, bar).isTrue)
            .toBe(true);
    });

        it('should let double negation work leftwards on an embedded formula',
            function() {
                var foo = BoolEditLanguage.formulaFactory("p + q''");
                var bar = BoolEditLanguage.formulaFactory("p + q");

                expect(BoolEditRules.getRule('T13b').doesJustify(foo, bar).isTrue)
                    .toBe(true);
    });

    it('should let double negation work rightwards on a complex formula',
        function() {
            var foo = BoolEditLanguage.formulaFactory('p + q');
            var bar = BoolEditLanguage.formulaFactory("(p + q)'");
            var rule = new BoolEditRules.__testonly__.Rule("foo", "x", "x'");

            expect(rule.doesJustify(foo, bar).isTrue)
                .toBe(true);

            var l = rule.doesJustify(foo, bar);
            expect (l.subMap.x.toString()).toEqual("p+q");
    });

    it('should let double negation work rightwards on a complex embedded formula', function() {
        var foo = BoolEditLanguage.formulaFactory('p + q * r');
        var bar = BoolEditLanguage.formulaFactory("p + (q * r)''");

        expect(BoolEditRules.getRule('T13b').doesJustify(foo, bar).isTrue)
            .toBe(true);
    });

    it('should let double negation work leftwards on a complex embedded formula', function() {
        var foo = BoolEditLanguage.formulaFactory("(q * r)'' + p");
        var bar = BoolEditLanguage.formulaFactory('q * r + p');

        expect(BoolEditRules.getRule('T13b').doesJustify(foo, bar).isTrue).toBe(true);
    });

    it('should let DeMorgans work leftwards on a complex embedded formula', function() {
        var foo = BoolEditLanguage.formulaFactory("(q * r)' + p ");
        var bar = BoolEditLanguage.formulaFactory("q' + r' + p ");

        expect(BoolEditRules.getRule('T15a').doesJustify(foo, bar).isTrue).toBe(true);
    });

    it('should deny DeMorons leftwards on a complex embedded formula', function() {
        var foo = BoolEditLanguage.formulaFactory("(q' * r)' + p ");
        var bar = BoolEditLanguage.formulaFactory("q'' * r' + p ");

        expect(BoolEditRules.getRule('T15a').doesJustify(foo, bar).isTrue).toBe(false);
    });

    it('should do something expected with one of the funny Unicode connectives', function() {
        var foo = BoolEditLanguage.formulaFactory("(P*Q*R*S'''''') ↓ (T*u')");
        var bar = BoolEditLanguage.formulaFactory("((P*Q*R*S'''''') + (T*u'))'");

        expect(BoolEditRules.getRule('df. ↓').doesJustify(bar, foo).isTrue).toBe(true);

    });
});